'''
Front end specific functions to create a Flask app via the application 
factory pattern.
'''

from time import time

from flask import g, render_template, request
from flask_login import current_user, logout_user
from flask_wtf.csrf import CSRFError

from crc_common.api import api_call
from crc_common_fe.models import User


def configure_hooks_fe(app, stats):
    '''
    Configure the before and after request hooks for front end apps
    
    Args:
        app (flask.Flask): The Flask application object.
        stats: The Flask StatsD extension object.
    '''

    @app.before_request
    def before_request():
        '''
        Performed before every web request to store the current user in g.user
        and start timing the request duration.
        '''

        # Time every request
        request.start_time = time()

        # Store the current user
        g.user = current_user
        
        # Logout inactive users (banned or locked out)
        if not g.user.is_active:
            logout_user()

    @app.after_request
    def after_request(response):
        '''
        Store prometheus metrics after every request.
        
        Returns:
            The Flask response object.
        '''

        latency = time() - request.start_time

        # Increment the endpoint hits by return code
        stats.incr(
            app.config['APP_NAME'].replace(
                '-', '_') + '_' + str(request.endpoint) + '_' + str(response.status_code)
        )

        # Time the latency for the endpoint
        stats.timing(
            app.config['APP_NAME'].replace(
                '-', '_') + '_' + str(request.endpoint) + '_latency_sec', latency
        )

        # Return the response unaltered
        return response


def set_error_handlers_fe(app):
    '''
    Configure front end error handlers. Note that all error handlers return an
    error template.
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    @app.errorhandler(401)
    def unauthorized_error(error):
        '''HTTP error handler for 401 errors'''

        app.logger.error(
            '401 Unauthorized : {err:s}'.format(err=error.description)
        )
        err_req = 'Unauthorized'
        return render_template('error.html', err_req=err_req), 401

    @app.errorhandler(403)
    def forbidden_error(error):
        '''HTTP error handler for 403 errors'''

        app.logger.error(
            '403 Forbidden : {err:s}'.format(err=error.description)
        )
        err_req = 'Forbidden'
        err_opt = 'You do not have permission to access this resource'
        return render_template('error.html', err_req=err_req, err_opt=err_opt), 403

    @app.errorhandler(404)
    def not_found_error(error):
        '''HTTP error handler for 404 errors'''

        err_req = 'File Not Found'
        err_opt = 'The requested resource was not found'
        return render_template('error.html', err_req=err_req, err_opt=err_opt), 404

    @app.errorhandler(413)
    def too_large_error(error):
        '''HTTP error handler for 413 errors'''

        app.logger.error(
            '413 Request Entity Too Large : {err:s}'.format(
                err=error.description)
        )
        err_req = 'Payload too large'
        err_opt = 'The data you tried to upload to the server is too large'
        return render_template('error.html', err_req=err_req, err_opt=err_opt), 404

    @app.errorhandler(500)
    def internal_error(error):
        '''HTTP error handler for 500 errors'''

        app.logger.critical(
            '500 Internal Server Error : {err:s}'.format(err=error.description)
        )
        err_req = 'An unexpected error has occurred'
        err_opt = 'We apologize for the inconvenience'
        return render_template('error.html', err_req=err_req, err_opt=err_opt), 500

    @app.errorhandler(CSRFError)
    def handle_csrf_error(error):
        '''CSRF error handler'''

        app.logger.warning('CSRF Error : ' + error.description)
        err_req = 'Your request could not be processed'
        return render_template('error.html', err_req=err_req), 400


def set_user_loader_fe(app, lm):
    '''
    Set the user loader for Flask-Login.
    
    Args:
        app (flask.Flask): The Flask application object.
        lm: The Flask-Login login manager object.
    '''

    def load_user(user_id):
        '''
        User loader function required by Flask-Login.
        
        Args:
            user_id (str): The unique identifer for the user.
            
        Returns:
            A user object if the user is found, otherwise None.
        '''

        try:
            # Call the "get" API
            resp = api_call(api='user-int-get', user_id=user_id)

            # Return a valid user object to Flask-Login
            return User(
                email=resp.get('user_email'),
                roles=resp.get('user_roles'),
                user_id=resp.get('user_id'),
                active=resp.get('active'),
                banned=resp.get('banned')
            )

        # Flask-Login expects 'None' to be returned for invalid IDs
        # Therefore, must catch all exceptions and return None
        except Exception as e:  # pylint: disable=broad-except
            app.logger.critical(str(e))
            return None

    lm.user_loader(load_user)
    lm.login_message = 'Please log in to access that page'
