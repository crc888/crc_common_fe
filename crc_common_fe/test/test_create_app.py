'''Unit tests for the create_app module'''

# Standard library imports
import unittest
from unittest.mock import patch

# Third party imports
from flask import Flask
from flask_login import LoginManager
from flask_wtf.csrf import CSRFError

# Local imports
from crc_common_fe.create_app import set_error_handlers_fe, set_user_loader_fe

# Global test data
LOGIN_MSG = 'Please log in to access that page'
USER_ID = '1'


class TestCreateApp(unittest.TestCase):
    '''Class containing all tests for the create_app function'''

    def setUp(self):
        '''
        Create a Flask object and Flask-Login login manager for all tests.
        '''
        
        self.app = Flask(__name__)
        self.test_app = self.app.test_client()
        self.lm = LoginManager()

    def test_error_handlers(self):
        '''Validate that the app has the desired error handlers.'''

        # Call the set_error_handlers_fe function
        set_error_handlers_fe(self.app)

        # Validate Standard HTTP error handlers
        assert 401 in self.app.error_handlers
        assert 403 in self.app.error_handlers
        assert 404 in self.app.error_handlers
        assert 413 in self.app.error_handlers
        assert 500 in self.app.error_handlers

        # Validate CSRF error handler
        err = CSRFError()
        assert self.app.error_handlers[400][type(err)] is not None

    @patch('crc_common_fe.create_app.api_call')
    def test_set_user_loader_fe(self, mock_api):
        '''
        Validate that the app's user loader is set properly.
        
        Args:
            mock_api (unittest.mock.MagicMock): Mock the api_call method
                called inside crc_common.create_app.
        '''

        # Mock the "user-int-get" API call
        mock_api.return_value = {
            'user_email': 'email.mail.com',
            'user_roles': [],
            'user_id': USER_ID,
            'active': True
        }

        # Call the set_user_loader_fe function
        set_user_loader_fe(self.app, self.lm)

        # Validate the login manager
        assert self.lm.user_callback is not None
        assert self.lm.login_message == LOGIN_MSG
