'''User class model required by Flask-Login.'''

from flask_login import UserMixin


class User(UserMixin):
    '''
    User class model required by Flask-Login
    
    Attributes:
        email (str): The user's email.
        user_id (str): The unique identifier of the user.
        roles (:obj:`list` of :obj:`str`): List of the user's roles.
        active (bool): True if the user is active. False otherwise.
        banned (bool): True if the user is banned. False otherwise.
    '''

    def __init__(self, email, user_id, roles, active, banned):
        self.email = email
        self.id = user_id
        self.roles = roles
        self.active = active
        self.banned = banned

    @property
    def is_active(self):
        '''
        Override the is_active method of Flask-Login UserMixin
        
        Returns:
            bool: True if the user is active and not banned, otherwise False.
        '''

        return self.active and not self.banned
