'''Install the crc_common_fe package.'''

from setuptools import setup

setup(
    name='crc_common_fe',
    version='0.4',
    description='Common modules for CRC front end services',
    author='Craig Ciccone',
    author_email='crc888@gmail.com',
    url='https://www.crc-web.cf/',
    packages=['crc_common_fe'],
    package_data={'crc_common_fe': ['templates/*']},
    dependency_links=[
        'https://bitbucket.org/crc888/crc_common/get/master.zip#egg=crc-common-0.2'
    ],
    install_requires=[
        'crc-common==0.3',
        'Flask-Login==0.4.0',
        'Flask-Moment==0.5.1',
        'Flask-WTF==0.14.2'
    ]
)
